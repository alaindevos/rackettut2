#lang typed/racket
(define (fib [n : Integer]) : Integer
  (if (> n 2)
      (+ (fib(- n 1)) (fib(- n 2)))
      n))
(display (fib 44))
