(import postgresql)
(import simple-loops)
(import pstk)
(import srfi-133)
(import format)
(import regex)
(import list-utils)
(define conn (connect '((host . "127.0.0.1")(dbname . "syslogng") (user . "x") (password . "x"))))
(define rows (query* conn "select * from messages_freebsd_20230416 order by datetime desc"))

(tk-start)
(ttk-map-widgets 'all)
; make sure we are using tile widget set
(tk/wm 'title tk "PS-Tk Example: tv")
(tk 'configure '#:height 1000 '#:width 1000)

(let (
	  [tv (tk 'create-widget
              'treeview
              '#:columns
              '("col0" "col1" "col2" "col3" "col4" "col5" "col6"))]
      [hsb (tk 'create-widget 'scrollbar '#:orient 'horizontal)]
      [vsb (tk 'create-widget 'scrollbar '#:orient 'vertical)]
      )
  (hsb 'configure '#:command (list tv 'xview))
  (vsb 'configure '#:command (list tv 'yview))
  (tv 'configure '#:xscrollcommand (list hsb 'set))
  (tv 'configure '#:yscrollcommand (list vsb 'set))
  (tv 'column "col0" '#:width 200)
  (tv 'heading "col0" '#:text "datetime")
  (tv 'column "col1" '#:width 100)
  (tv 'heading "col1" '#:text "host")
  (tv 'column "col2" '#:width 100)
  (tv 'heading "col2" '#:text "program")
  (tv 'column "col3" '#:width 100)
  (tv 'heading "col3" '#:text "pid")
  (tv 'column "col4" '#:width 100)
  (tv 'heading "col4" '#:text "facility")
  (tv 'column "col5" '#:width 100)
  (tv 'heading "col5" '#:text "priority")
  (tv 'column "col6" '#:width 800)
  (tv 'heading "col6" '#:text "message")
  (row-for-each
    (lambda (x)
      (let* ((mytv tv)) (mytv 'insert "" 'end '#:text "" '#:values x)))
    rows)
  (tk/grid tv '#:column 0 '#:row 0 '#:sticky 'news)
  (tk/grid hsb '#:column 0 '#:row 1 '#:sticky 'we)
  (tk/grid vsb '#:column 1 '#:row 0 '#:sticky 'ns)
  (tk/grid 'columnconfigure tk 0 '#:weight 1)
  (tk/grid 'rowconfigure tk 0 '#:weight 1)
  (tk-event-loop)
  );let
