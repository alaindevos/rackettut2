#lang typed/racket
(: mysum : Number Number * -> Number )
(define (mysum x . y)
    (cond
        [(null? y) x]
        [(null? (cdr y)) (+ x (car y))]
        [else (+ (car y) (apply mysum (cdr y)))]))
(print (mysum 1 2 3 4 5 6 7))
