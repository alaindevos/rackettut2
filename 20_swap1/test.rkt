#lang typed/racket

( : myswap : (Pairof Integer Integer) -> (Pairof Integer Integer))
(define myswap
  (lambda (p)
    (match p
      [(cons x y) (cons y x)]
      )
    )
  )
(define x (myswap (cons 1 2)))
(: y : Integer)
(define y (car x))
(: z : Integer)
(define z (cdr x))
(writeln y)
(writeln z)
