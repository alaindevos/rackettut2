#lang typed/racket
(require  racket/match )
(define-struct myoperand ([ O : [U 'plus 'minus] ]) #:transparent )
(define-struct myleaf    ([ L : Number]) #:transparent )
(define-struct mynode    ([ OP : myoperand ] [ E1 : myexpr ] [ E2 : myexpr ] ) #:transparent )
(define-struct myexpr    ([ E : (U myleaf mynode mycnd)]) #:transparent )
(define-struct mycnd     ([ MYCND : myexpr ] [ MYTHEN : myexpr] [ MYELSE : myexpr ]) #:transparent )

(: myeval : myexpr -> Number)
(define myeval
  (lambda (x)
    [match (myexpr-E x)
      ([myleaf L] L );case
      ([mynode OP E1 E2]
       (if (eq? (myoperand-O OP) 'plus )
         (+ (myeval E1) (myeval E2) );+
         -1000;else
         ))
      ([mycnd MYCND MYTHEN MYELSE]
       (if (eq? MYCND 0)
         (myeval MYELSE)
         (myeval MYTHEN)))
      ]))

(: to_expra : Number -> myexpr )
(define to_expra
  (lambda (x)
    [let* ([l (make-myleaf x)]
           [e  (make-myexpr l)]
         );let
      e
      ];let
    );lambda
  );define


(: to_exprb : ( U 'minus 'plus ) Number Number -> myexpr )
(define to_exprb
  (lambda (x1 x2 x3)
    [let* ([op (make-myoperand x1)]
           [l1 (make-myleaf x2)]
           [l2 (make-myleaf x3)]
           [n  (make-mynode op (make-myexpr l1) (make-myexpr l2))]
           [e  (make-myexpr n)]
         );let
      e
      ];let
    );lambda
  );define


(: to_exprc : ( U 'minus 'plus ) myexpr myexpr -> myexpr )
(define to_exprc
  (lambda (x1 e1 e2)
    [let* ([op (make-myoperand x1)]
           [n  (make-mynode op e1 e2)]
           [e  (make-myexpr n)]
         );let
      e
      ];let
    );lambda
  );define

(write (myeval (to_expra 1)))
(write (myeval (to_exprb 'plus 2 3)))
(write (myeval (to_exprc 'plus (to_exprb 'plus 4 5) (to_exprb 'plus 6 7))))
(write (myeval (make-myexpr (make-mycnd (to_expra 1) (to_expra 2 ) (to_expra 3)))))
