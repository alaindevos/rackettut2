#lang typed/racket

( : mysum : (Listot Number) -> Number )
(define mysum
  (lambda (x)
    [match x
      ['() 0]
      [(cons h t) (+ h (mysum t))]
      ];match
    );lambda
  );define
(write (mysum (list 1 2 3)))
